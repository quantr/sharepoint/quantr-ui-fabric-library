import * as React from 'react';
import {
	CompoundButton
} from 'office-ui-fabric-react/lib/Button';
import 'office-ui-fabric-core/dist/css/fabric.min.css';
// import IQuantrTabComponent from './IQuantrTabComponent';

export default class QuantrTab extends React.Component<{ tabNames: string[], tabDescriptions: string[], tabIcons: string[], tabComponents: any }, {}> {
	constructor(props: any) {
		super(props);
		this.state = { tabNames: this.props.tabNames };
	}

	public render(): React.ReactElement<{}> {
		// var classNames:any = require('classnames');

		const buttons = [];
		const elements = [];
		for (let x = 0; x < this.props.tabNames.length; x++) {
			buttons.push(
				<div className="ms-Grid-col ms-sm12 ms-md12 ms-lg6 ms-xl2" style={{ paddingLeft: '0px', paddingRight: '2px' }}>
					<CompoundButton
						primary={true}
						description={this.props.tabDescriptions[x]}
						iconProps={{ iconName: this.props.tabIcons[x] }}
						style={{ margin: 0, width: '100%', whiteSpace: 'nowrap' }}
						onClick={this.handleClick.bind(this, x)}
					>
						{this.props.tabNames[x]}
					</CompoundButton>
				</div>);
			elements.push(React.createElement(this.props.tabComponents[x], { display: (x === 0 ? '' : 'none'), ref: 'element_' + x }));
		}

		return (
			<div>
				<div className="ms-Grid">
					<div className="ms-Grid-row">
						{buttons}
					</div>
				</div>
				{elements}
			</div>
		);
	}

	private handleClick(x: number) {
		for (let z = 0; z < this.props.tabComponents.length; z++) {
			const ele: any = this.refs['element_' + z];
			ele.setVisible(false);
		}
		debugger
		const ele2: any = this.refs['element_' + x];
		ele2.setVisible(true);
	}
}
