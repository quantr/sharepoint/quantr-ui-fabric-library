import * as React from 'react';
import './App.css';

import QuantrTab from './QuantrTab';
import { initializeIcons } from '@uifabric/icons';
import { loadTheme } from 'office-ui-fabric-react/lib/Styling';
import QuantrTabComponent from './QuantrTabComponent';

class Dashboard extends QuantrTabComponent {
	public render() {
		return <div style={{ display: this.state.display }}>Dashboard</div>;
	}
}

class Lead extends QuantrTabComponent {
	public render() {
		return <div style={{ display: this.state.display }}>Lead</div>;
	}
}

class App extends React.Component {
	constructor(props: any) {
		super(props);
		loadTheme({
			palette: {
				'themePrimary': '#3e0e48'
			}
		});
		initializeIcons();
	}

	public render() {
		return (
			<div className="App">
				<header className="App-header">
					<h1 className="App-title">Quantr Fabric Library</h1>
					<h2>Extending Microsoft Office UI Fabric Library</h2>
					<a href="https://developer.microsoft.com/en-us/fabric" target="_blank">https://developer.microsoft.com/en-us/fabric</a>
				</header>
				<QuantrTab
					tabNames={["Dashboard", "Lead", "Opportunity", "Activity", "Timeline", "Setting"]}
					tabDescriptions={['An overview', 'Any Fish in The Sea', 'Nemo', 'Activity management', 'Manage every about time', 'All settings go here']}
					tabIcons={['ViewDashboard', 'AccountManagement', 'Money', 'DateTime', 'DateTime', 'Settings']}
					tabComponents={[Dashboard, Lead, Dashboard, Lead, Dashboard, Lead]}
				/>
				<div className="App-footer">
					Contact us <a href="https://www.quantr.hk" target="_blank">www.quantr.hk</a>
				</div>
			</div>
		);
	}
}

export default App;
