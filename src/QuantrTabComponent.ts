
import * as React from 'react';
import IQuantrTabComponent from './IQuantrTabComponent';

export default class QuantrTabComponent extends React.Component<{ display: string }, { display: string }> implements IQuantrTabComponent {
	constructor(props: any) {
		super(props);
		this.state = { display: this.props.display };
	}
	
	public setVisible(b: boolean) {
		if (b) {
			this.setState({ display: '' });
		} else {
			this.setState({ display: 'none' });
		}
	}
}